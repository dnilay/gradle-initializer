package com.example.gradle.initializr.generator;

import com.example.gradle.initializr.model.ProjectRequest;

import java.io.File;

public interface ProjectGenerator {

    void generate(File targetDir, ProjectRequest projectRequest);
}
