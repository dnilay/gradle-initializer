package com.example.gradle.initializr.service;

import com.example.gradle.initializr.model.ProjectRequest;

import java.util.List;

public interface GradleInitializrService {

    byte[] createZip(ProjectRequest projectRequest);
    byte[] createTgz(ProjectRequest projectRequest);
    List<AnnotatedGradleVersion> getGradleVersions();
}
